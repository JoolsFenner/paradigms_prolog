%http://www.ic.unicamp.br/~meidanis/courses/mc336/2009s2/prolog/problemas/
%


%P01 last element of a list

my_last(X,[X]).
my_last(X,[_|T]) :-
	my_last(X,T).

%P02 last but one element of a list

my_last_b1(X, [X,_]).
my_last_b1(X, [_|T]) :-
	my_last_b1(X,T).

%P03 kth element of a list
element_at(X,[X|_],1).
element_at(X, [_|L], N) :-
	N > 1,
	N1 is N - 1,
	element_at(X, L, N1).

%p04 count element in list

%p05 reverse list
my_reverse(L1,L2) :- my_rev(L1,L2,[]).

my_rev([],L2,L2) :- !.
my_rev([X|Xs],L2,Acc) :-
	my_rev(Xs,L2,[X|Acc]).

%P06  is list a palindrome?
palindrome([]).
palindrome([H|T]) :-
	H = my_last(T),
	palindrome(T).

