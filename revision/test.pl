lexless([],[_|_]).
lexless([H1|_], [H2|_]) :- H1 < H2.
lexless([H1|T1],[H2|T2]) :-
	H1 = H2,
	lexless(T1, T2).
