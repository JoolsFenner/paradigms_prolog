fib(0,1).
fib(1,1).
fib(N,Result) :-
	A is N - 1,
	B is N - 2,
	fib(A, Result1),
	fib(B, Result2),
	Result is Result1 + Result2.


lstlength([],0).
lstlength([_|T],Result) :-
	lstlength(T, TempResult),
	Result is TempResult + 1.

sumlst([],0).
sumlst([H|T], Result) :-
	sumlst(T, TempResult),
	Result is TempResult + H.


averageLst(L,Result) :-
	sumlst(L, SumResult),
	lstlength(L, LengthResult),
        Result is SumResult / LengthResult.


lexless([],[_|_]).
lexless([H1|_], [H2|_]) :- H1 < H2.
lexless([H1|T1],[H2|T2]) :-
	H1 = H2,
	lexless(T1, T2).

tester([H|T]) :-
	write(H), nl,
	write(T), nl.

append([], L, L).
append([H|T], L2, [H|L3]) :-
	append(T, L2, L3).

prefix(X,Y) :-
	append(X, _ , Y).

suffix(X,Y) :-
	append(_, X , Y).


duplicate([X, X|_], X).
duplicate([_|Tail], X) :- duplicate(Tail, X).


revAppend([],X,X).
revAppend([X|Y], Z, W) :-
	 revAppend(Y, [X|Z], W).
