go(From, To, Path):-
  go(From, To, [], Path).

go(X, X, T, T).
go(X, Y, T, NT) :-
    (d(X,Z) ; d(Z, X)),
    \+ member(Z,T),
    go(Z, Y, [Z|T], NT).
