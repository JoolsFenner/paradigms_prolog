/*
using cut operator 1
try with foo.

*/

foo :- a,b.
foo :- c,d,!,e,f.
foo :- g,h.

a.
b.
c.
d.
e.
f.
g.
h.
